OpenLDAP hashing schemes
------------------------

To use existing passwords, two hashing schemes were added:

* BCRYPT - using a standard implementation
* PIR_H9 and PIR_HI - custom MD5-based hash ported from phpBB

BCrypt
------
The default scheme `$2y$` was hardcoded for creating new passwords in `libbcrypt/bcrypt.c::bcrypt_gensalt`.

PiratiHash
----------
The original PHP source is included with debug output for reference.

Compilation
-----------
- download OpenLDAP 2.4.44, compile with `./configure --with-tls=gnutls`
- place 'bcrypt' and 'piratiHash' dirs into 'openldap-2.4.44/contrib/slapd-modules/passwd'
- compile using make
- these packages were needed to compile on Debian:
  -  make gcc libldap2-dev libtool libgnutls28-dev
  -  libdb5.3-dev libcrypto++-dev libssl1.0-dev

Deployment
----------
- place compiled library from .libs into OpenLDAP module dir (/usr/lib/ldap) :
  - pw-pirati.la  
  - pw-pirati.so
  - pw-pirati.so.0
  - pw-pirati.so.0.0.0
- tell OpenLDAP about the modules using ldapmodify:

```bash
ldapmodify -Y EXTERNAL -H ldapi:/// <<<LDIF
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: pw-pirati
-
add: olcModuleLoad
olcModuleLoad: pw-bcrypt
LDIF
```
- (similarly for bcrypt)
