/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 2009-2016 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */
/* ACKNOWLEDGEMENT:
 * This work was initially developed by HAMANO Tsukasa <hamano@osstech.co.jp>
 */

/**
 *      Adapted by Martin Rejman, 25.10.2017
 *
 *      apt-get install: make gcc libldap2-dev libtool
 *                       libgnutls28-dev libdb5.3-dev libcrypto++-dev
 *                       libssl1.0-dev
 */

#define _GNU_SOURCE

#include "portable.h"
#include <ac/string.h>
#include "lber_pvt.h"
#include "lutil.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_OPENSSL
#include <openssl/evp.h>
#elif HAVE_GNUTLS
#include <nettle/pbkdf2.h>
#include <nettle/hmac.h>
typedef void (*pbkdf2_hmac_update)(void *, unsigned, const uint8_t *);
typedef void (*pbkdf2_hmac_digest)(void *, unsigned, uint8_t *);
#else
#error Unsupported crypto backend.
#endif

#include "lutil_md5.h"

#define SALT_SIZE 8

const char* itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

// for hash scheme of $H$9......
const struct berval pirate_h9_scheme = BER_BVC("{PIR_H9}");
// for hash scheme of $H$I......
const struct berval pirate_hi_scheme = BER_BVC("{PIR_HI}");


int pirate_hash_encode64(
                const struct berval *scheme,
                const struct berval *digest,
                struct berval *b64,
                const struct berval *salt,
                const int count_log2)
{
#ifdef SLAPD_PIRATE_DEBUG
	printf("\n");
	printf("  pirate_hash_encode64():\n");
	printf("    scheme:\t\t%s\n", scheme->bv_val);
	printf("    digest: " , digest->bv_val);
        for(int i=0; i<digest->bv_len; i++){ printf("%02x", digest->bv_val[i] & 0xff);}
        printf("\n");

	printf("    salt:\t\t");
        for(int i=0; i<salt->bv_len; i++){ printf("%c", salt->bv_val[i]);}
	printf("\n");
        printf("    count_log2: %d\n", count_log2);
#endif
        /**
         * Reimplementation from PHP for $H$I hashes
         */

        const int count = 16;

        char* input = digest->bv_val;
        struct berval output;
        output.bv_len = 70; // 11 'scheme' + 34 'origHash' should be enough
        output.bv_val = ber_memalloc( output.bv_len );
        int pos = 0;

        // copy scheme name to output
        AC_MEMCPY( output.bv_val, scheme->bv_val, scheme->bv_len );
        pos += scheme->bv_len;

        // set the '$H$I' prefix
        AC_MEMCPY( &output.bv_val[pos], "$H$", 3 );
        pos += 3;
        AC_MEMCPY( &output.bv_val[pos], &itoa64[count_log2], 1 );
        pos += 1;

        // copy the salt value
        AC_MEMCPY( &output.bv_val[pos], salt->bv_val, salt->bv_len );
        pos += salt->bv_len;

        int i = 0;
        int value = 0;

        do
        {
                // php // $value = ord($input[$i++]);
#ifdef SLAPD_PIRATE_DEBUG2
                printf("%02x\n",input[i]);
#endif
                value = (char) input[i++] & 0xff;

                // php // $output .=   $itoa64[$value & 0x3f];
                output.bv_val[pos++] = itoa64[ value & 0x3f ];
#ifdef SLAPD_PIRATE_DEBUG2
                printf("A %c\n",itoa64[ value & 0x3f ]);
#endif

                // php // if ($i < $count)
                // php // {
                // php //      $value |= ord($input[$i]) << 8;
                // php // }
                if( i < count )
                {
#ifdef SLAPD_PIRATE_DEBUG2
                        printf("%d\n",value);
#endif
                        value |= ((int)(input[i]) & 0xff) << 8;
                }

                // php // $output .= $itoa64[($value >> 6) & 0x3f];
                output.bv_val[pos++] = itoa64[(value >> 6) & 0x3f];
#ifdef SLAPD_PIRATE_DEBUG2
                printf("B %c\n", itoa64[(value >> 6) & 0x3f]);
#endif

                // php // if ($i++ >= $count)
                // php // {
                // php //         break;
                // php // }
                if( i++ >= count )
                {
                        break;
                }

                // php // if ($i < $count)
                // php // {
                // php //         $value |= ord($input[$i]) << 16;
                // php // }
                if( i < count )
                {
                        value |= ((int)(input[i]) & 0xff) << 16;
                }


                // php // $output .=  $itoa64[($value >> 12) & 0x3f];
                output.bv_val[pos++] = itoa64[(value >> 12) & 0x3f];
#ifdef SLAPD_PIRATE_DEBUG2
                printf("C %c\n", itoa64[(value >> 12) & 0x3f]);
#endif

                // php // if ($i++ >= $count)
                // php // {
                // php //         break;
                // php // }
                if( i++ >= count )
                {
                        break;
                }

                // php // $output .=  $itoa64[($value >> 18) & 0x3f];
                output.bv_val[pos++] = itoa64[(value >> 18) & 0x3f];
#ifdef SLAPD_PIRATE_DEBUG2
                printf("D %c\n", itoa64[(value >> 18) & 0x3f]);
#endif
        } while ( i < count );

        int final_length = pos -1 +1;   // -1 for last pos++, +1 for length

	b64->bv_len = final_length;
	b64->bv_val = ber_memalloc( final_length + 1 );

	if( b64->bv_val == NULL ) {     // memalloc error ?
		ber_memfree( output.bv_val );
		return LUTIL_PASSWD_ERR;
	}

	AC_MEMCPY(b64->bv_val, output.bv_val, final_length);
        b64->bv_val[final_length] = 0x00;


        ber_memfree( output.bv_val );
        
#ifdef SLAPD_PIRATE_DEBUG
        printf("    b64: %s\n", b64->bv_val);
        printf("\n");
        printf("    Assertion check: strlen=%d, bv_len=%d\n", 
                        strlen(b64->bv_val), (int)b64->bv_len );
#endif

        assert( strlen(b64->bv_val) == b64->bv_len );

        return LUTIL_PASSWD_OK;
}



static int pirate_check(
                const struct berval *scheme,
                const struct berval *passwd,
                const struct berval *cred,
                const char **text,
                const int count_log2)
{
        /**
         * This hash has the form of $H$Csssssssshhhhhh...hhhhh
         * where:       
         *      H is just th letter
         *      C is 9 or I meaning 2^11 or 2^20 iterations
         *      s{8} is the salt
         *      hh   is the hash
         */
#ifdef SLAPD_PIRATE_DEBUG
	printf("\n");
        printf("pirate_check():\n");
        printf("  scheme: %s\n", scheme->bv_val);
        printf("  passwd: %s\n", passwd->bv_val);
        printf("  credential: %s\n", cred->bv_val);
        printf("  count_log2: %d\n", count_log2);
#endif

        lutil_MD5_CTX   MD5context;
        unsigned char   MD5digest[LUTIL_MD5_BYTES];
        char            saltdata[SALT_SIZE];
        struct berval digest;
        struct berval salt;
        int count = 1 << count_log2;

        // init structures
        digest.bv_val = (char *) MD5digest;
        digest.bv_len = sizeof(MD5digest);
        salt.bv_val = saltdata;
        salt.bv_len = sizeof(saltdata);

        // extract salt value s{8} from hash $H$Csssssssshhhhh....hhh
        AC_MEMCPY( &salt.bv_val[0], &passwd->bv_val[4], salt.bv_len );

#ifdef SLAPD_PIRATE_DEBUG
	printf("  salt found:\t");
        for(int i=0; i<salt.bv_len; i++){ printf("%c", salt.bv_val[i]);}
	printf("\n");
#endif

        // MD5 of ( salt + password )
        lutil_MD5Init( &MD5context );
        lutil_MD5Update( &MD5context,
                        (const unsigned char *) salt.bv_val, salt.bv_len );
        lutil_MD5Update( &MD5context,
                        (const unsigned char *) cred->bv_val, cred->bv_len );
        lutil_MD5Final( MD5digest, &MD5context );

#ifdef SLAPD_PIRATE_DEBUG
	printf("  first md5: " , digest.bv_val);
        for(int i=0; i<digest.bv_len; i++){ printf("%02x", digest.bv_val[i] & 0xff);}
        printf("\n");
#endif

        do {
                lutil_MD5Init( &MD5context );
                lutil_MD5Update( &MD5context,
                                (const unsigned char *) digest.bv_val, digest.bv_len );
                lutil_MD5Update( &MD5context,
                                (const unsigned char *) cred->bv_val, cred->bv_len );
                lutil_MD5Final( MD5digest, &MD5context );
        } while(--count);

#ifdef SLAPD_PIRATE_DEBUG
	printf("  final md5: " , digest.bv_val);
        for(int i=0; i<digest.bv_len; i++){ printf("%02x", digest.bv_val[i] & 0xff);}
        printf("\n");
#endif


        // format the HASH using the values
        struct berval newHash;
        pirate_hash_encode64( scheme, &digest, &newHash, &salt, count_log2 );

        // compare to the given hash with new one
	int rc = memcmp(passwd->bv_val, &newHash.bv_val[scheme->bv_len], passwd->bv_len);

#ifdef SLAPD_PIRATE_DEBUG
	printf("\n");
        printf("  compare passwd:\t%s\n", passwd->bv_val);
        printf("    newHash[%d:]:\t%s\n", scheme->bv_len, &newHash.bv_val[scheme->bv_len]);
#endif

	ber_memfree(newHash.bv_val);

	return rc ? LUTIL_PASSWD_ERR : LUTIL_PASSWD_OK;


}

static int pirate_encrypt(
                const struct berval *scheme,
                const struct berval *passwd,
                //struct berval *msg,
                struct berval *hash,
                const char **text,
                const int count_log2)
{
#ifdef SLAPD_PIRATE_DEBUG
	printf("\n");
        printf("pirate_encrypt():\n");
        printf("  scheme: %s\n", scheme->bv_val);
        printf("  passwd: %s\n", passwd->bv_val);
        printf("  hash: %s\n", hash->bv_val);
        printf("  count_log2: %d\n", count_log2);
#endif

        lutil_MD5_CTX   MD5context;
        unsigned char   MD5digest[LUTIL_MD5_BYTES];
        char            saltdata[SALT_SIZE];
        struct berval digest;
        struct berval salt;
        int count = 1 << count_log2;

        // init structures
        digest.bv_val = (char *) MD5digest;
        digest.bv_len = sizeof(MD5digest);
        salt.bv_val = saltdata;
        salt.bv_len = sizeof(saltdata);

        // get new salt value
        if( lutil_entropy( (unsigned char *) salt.bv_val, salt.bv_len) < 0 ) {
                return LUTIL_PASSWD_ERR; 
        }
        // make salt characters ASCII
        for(int i=0; i<salt.bv_len; i++) {
                salt.bv_val[i] = itoa64[ ((int) salt.bv_val[i]) % strlen(itoa64)] ;
        }

#ifdef SLAPD_PIRATE_DEBUG
        // If we want to hardcode hash for testing.
        //AC_MEMCPY( salt.bv_val, "84478476", 8);
#endif

        // MD5 of ( salt + password )
        lutil_MD5Init( &MD5context );
        lutil_MD5Update( &MD5context,
                        (const unsigned char *) salt.bv_val, salt.bv_len );
        lutil_MD5Update( &MD5context,
                        (const unsigned char *) passwd->bv_val, passwd->bv_len );
        lutil_MD5Final( MD5digest, &MD5context );

#ifdef SLAPD_PIRATE_DEBUG
	printf("  first md5: " , digest.bv_val);
        for(int i=0; i<digest.bv_len; i++){ printf("%02x", digest.bv_val[i] & 0xff);}
        printf("\n");
#endif


        do {
                lutil_MD5Init( &MD5context );
                lutil_MD5Update( &MD5context,
                                (const unsigned char *) digest.bv_val, digest.bv_len );
                lutil_MD5Update( &MD5context,
                                (const unsigned char *) passwd->bv_val, passwd->bv_len );
                lutil_MD5Final( MD5digest, &MD5context );
        } while(--count);

#ifdef SLAPD_PIRATE_DEBUG
        printf("\n");
#endif

        return pirate_hash_encode64( scheme, &digest, hash, &salt, count_log2 );
}

static int pirate_h9_check(
                const struct berval *scheme,
                const struct berval *passwd,
                const struct berval *cred,
                const char **text)
{
        return pirate_check(scheme, passwd, cred, text, 11);
}

static int pirate_h9_encrypt(
                const struct berval *scheme,
                const struct berval *passwd,
                struct berval *cred,
                const char **text)
{
        return pirate_encrypt(scheme, passwd, cred, text, 11);
}

static int pirate_hi_check(
                const struct berval *scheme,
                const struct berval *passwd,
                const struct berval *cred,
                const char **text)
{
        return pirate_check(scheme, passwd, cred, text, 20);
}

static int pirate_hi_encrypt(
                const struct berval *scheme,
                const struct berval *passwd,
                struct berval *cred,
                const char **text)
{
        return pirate_encrypt(scheme, passwd, cred, text, 20);
}

int init_module(int argc, char *argv[]) {
        int rc;

        rc = lutil_passwd_add((struct berval *)&pirate_h9_scheme,
                        pirate_h9_check, pirate_h9_encrypt);
        if(rc) return rc;

        rc = lutil_passwd_add((struct berval *)&pirate_hi_scheme,
                        pirate_hi_check, pirate_hi_encrypt);
        return rc;
}

/*
 * Local variables:
 * indent-tabs-mode: t
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 */
