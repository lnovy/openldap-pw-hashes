<?php

/**
* Check for correct password
*
* @param string $password The password in plain text
* @param string $hash The stored password hash
*
* @return bool Returns true if the password is correct, false if not.
*/
function phpbb_check_hash($password, $hash)
{
	if (strlen($password) > 4096)
	{
		// If the password is too huge, we will simply reject it
		// and not let the server try to hash it.
		return false;
	}

	$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	if (strlen($hash) == 34)
	{
		return (_hash_crypt_private($password, $hash, $itoa64) === $hash) ? true : false;
	}

	return (md5($password) === $hash) ? true : false;
}


/**
* The crypt function/replacement
*/
function _hash_crypt_private($password, $hash_orig, &$itoa64)
{
	$output = '*';

	// Check for correct hash
	if (substr($hash_orig, 0, 3) != '$H$' && substr($hash_orig, 0, 3) != '$P$')
	{
		return $output;
	}

	$count_log2 = strpos($itoa64, $hash_orig[3]);

	if ($count_log2 < 7 || $count_log2 > 30)
	{
		return $output;
	}

	$count = 1 << $count_log2;
	$salt = substr($hash_orig, 4, 8);

	if (strlen($salt) != 8)
	{
		return $output;
	}

        //print("salt : $salt\n");

	/**
	* We're kind of forced to use MD5 here since it's the only
	* cryptographic primitive available in all versions of PHP
	* currently in use.  To implement our own low-level crypto
	* in PHP would result in much worse performance and
	* consequently in lower iteration counts and hashes that are
	* quicker to crack (by non-PHP code).
	*/
	if (PHP_VERSION >= 5)
	{
		$hash = md5($salt . $password, true);
                //print("First hash: ".md5($salt . $password, false)."\n");
		do
		{
			$hash = md5($hash . $password, true);
		}
		while (--$count);
	}
	else
	{
		$hash = pack('H*', md5($salt . $password));
		do
		{
			$hash = pack('H*', md5($hash . $password));
		}
		while (--$count);
	}

        //print("Final hash: ".bin2hex($hash)."\n");
	$output = substr($hash_orig, 0, 12);
	$output .= _hash_encode64($hash, 16, $itoa64);

        //print("Hash orig: ".$hash_orig."\n");
        //print("Hash new : ".$output."\n");
	return $output;
}

/**
* Encode hash
*/
function _hash_encode64($input, $count, &$itoa64)
{
	$output = '';
	$i = 0;

	do
	{
            //print(bin2hex($input[$i])."\n");
		$value = ord($input[$i++]);
                //print("A ".$itoa64[$value & 0x3f]."\n");
		$output .= $itoa64[$value & 0x3f];

		if ($i < $count)
		{
                    //print($value."\n");
			$value |= ord($input[$i]) << 8;

		}

		//print("B ".$itoa64[($value >> 6) & 0x3f] . "\n");
		$output .= $itoa64[($value >> 6) & 0x3f];

		if ($i++ >= $count)
		{
			break;
		}

		if ($i < $count)
		{
			$value |= ord($input[$i]) << 16;
		}

		//print("C ".$itoa64[($value >> 12) & 0x3f] . "\n");
		$output .= $itoa64[($value >> 12) & 0x3f];

		if ($i++ >= $count)
		{
			break;
		}

		//print("D ".$itoa64[($value >> 18) & 0x3f] . "\n");
		$output .= $itoa64[($value >> 18) & 0x3f];
	}
	while ($i < $count);

	return $output;
}

$test_hashes = Array(
    'ahoj' => Array(
        '$H$I0LKjZaxBxqBBu8tKFG0rErfzeyQ2h1',
        '$H$I123456783GKzj/uDpNK77I.jH5Wga.',
        '$H$I29/jIG9idiKD11AJ2xNWaeCc1bjDn.',
        /*
        '$H$I8pdt3v6WKKXp8vWj.sUlcqwbJUwbM0',
        '$H$I9C.nZdnmlqjpVnq.50FWKrdINbbAW/',
        '$H$I9gOhVXGBFmLiHux6g2oluR7ko4jKs1',
        '$H$IDpa3Vr1tpBniZiD6Q76.eQP4XA80P0',
        '$H$IFJI7ZEhppps1QgpljSh6QVnie4e.Y/',
        '$H$IFgL0LRMo5bzlSTFKJAZg281xWT9tG1',
        '$H$IGiQRGbm3tvlRNNNC.fjJ3bA8iuM/S.',
        '$H$IM6zGUQZL1sFe3b2PCTNmxUCU7rv4q/',
        '$H$IODdFTlX/i2nvLlPJ5pMQEe0ojUgVp0',
        '$H$IP9uYfOxW2k8zFWUXUpBsGL9eeHHxM0',
        '$H$IQfnWqT138ppYr/trWV1GfjmSGHd71/',
        '$H$IRowyw7J6eqmShk.D2SK1gt0L.qWpy/',
        '$H$IUc1POfrWMqY0QhVWGw3ksGlX6mA24/',
        '$H$IUyUEEddPdtMZyUuzH1mtE/LmwqsXD/',
        '$H$IVCAZJp5uO8h/eV8X/C230G4MdhFXn.',
        '$H$IWcrv3gJPUCelOI5R.2rsG5NOTz66c/',
        '$H$IYRnI0Wzwr6.KWQqZrUPqND68hvwLr1',
        '$H$IZmr5r5TOJ9abwKkl3W0vdueD9OB6Q1',
        '$H$Ia3o2ZMvVYvX.zcNoq516G.IMTJiMd.',
        '$H$IbLxobCgyYH3iSDFI1O7fS6TYkYN000',
        '$H$IdOJ8Bpzv.gLb10PazSQKgGZPBS6io0',
        '$H$IeOwN2D6eAQ5Rwh264b3uaE.UCsN9E/',
        '$H$IhtG2F6u3g3AjsXJY1tEIvNAEM7Uih/',
        '$H$IiVIupy69lnkFHNDrs/Ez8x0tWVg8t1',
        '$H$IjAeNOlkdycxuIT6QWxlRc0u/ThyW5.',
        '$H$ImNSpo5yG/2YYRprtPgTv3Mb8ywdnW.',
        '$H$IowbaLYZv8Qdxuh94fVNar0P0vuQuC0',
        '$H$It4V1YoJbR6rq9notGYiyzcDXABu900',
        '$H$ItZ.hVTUGtRLdnsEocgye2c3dtX04R.',
        '$H$Iy8HsuIdLqMxS3jJs7VTL/A6mLfsQJ/',
         */
    ),
    'hashcat' => Array(
        '$H$984478476IagS59wHZvyQMArzfx58u.',
        '$H$I6BGgFrNiWpdsYmbON1IdC32vFJOG/0',
        '$H$I2B1QMAQ1ZfEq68iUsX9PKZM86dt1n1',
        '$H$Iz1mxeGkKsaqKuW3sYisW67Hg0cALH0',
        '$H$Ihn4b4FVSriyNMauLKK4xO4sDwZzmR0',
        '$H$9S5AJfKwMjjMG3iIt.bsdG1AO4zajf/',
        '$H$9Lsi1ugryfiY2.Fb/1coYbBcmleV511',
    )
);

foreach( $test_hashes as $passwd => $hashes)
{
    print($passwd."\n");

    foreach( $hashes as $hash ){
        print('  {PIRMD5_HI}'.$hash.'  ');
        if( phpbb_check_hash($passwd,$hash) ){
            print("Pass\n");
        } else {
            print("Fail\n");
            break;
        }
    }
}

?>
